﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using System.Threading;
using TMS.TrackingScheduler._customObj;
using System.Configuration;
using static TMS.TrackingScheduler._customObj.CommonConstanta;
using static TMS.TrackingScheduler._customObj.PostClientHistory;
//using static TMS.TrackingScheduler._customObj.APIParam;
using System.Reflection;
using Newtonsoft.Json;

namespace TMS.TrackingScheduler
{
    class Program
    {
        #region Defined Parameter
        private enum ExecMethod
        {
            DEFINED_PERIOD,
            SPECIFIC_DATE,
            SPECIFIC_STT,
            DEFAULT
        }
        #endregion

        #region Properties
        private static Mutex mutex = null;
        static ResiTrackings _resiTrackings = new ResiTrackings();
        static PostClientHistories _postClientHistories = new PostClientHistories();
        static string _isStoreToDataTable = ConfigurationManager.AppSettings["IsStoreToDataTable"];
        private static ExecMethod _execMethod = ExecMethod.DEFAULT;

        private static Type _type;
        private static MethodInfo _methodInfo;
        #endregion

        static void IntegrationMain(string[] args)
        {
            try
            {
                PartnerSetting.SetPartnerConfiguration();

                Assembly a = Assembly.LoadFile($"{Directory.GetCurrentDirectory()}\\{PartnerSetting.PostAssembly}.dll");
                _type = a.GetType($"{PartnerSetting.PostAssembly}.PostToPartnerService");
                _methodInfo = _type.GetMethod("Post");

                writeLog($"Console Integration [{PartnerSetting.PartnerName}] STARTED at [{DateTime.Now}]"); //attemp to check how many minute passed before DB timeout  

                DataTable _dataToBePost = getOutstandingTrackingData(args);
                foreach (DataRow _dRow in _dataToBePost.Rows)
                {
                    ResiTracking _tracking = new ResiTracking();
                    _tracking.CustPackageId = _dRow["Cust_Package_Id"].ToString();
                    _tracking.ReceiptNumber = _dRow["ReceiptNumber"].ToString();
                    _tracking.TrackingDatetime = Convert.ToDateTime(_dRow["TrackingDatetime"]);
                    _tracking.Notes = _dRow["note"].ToString();
                    _tracking.PostClientHistoryId = _dRow["id"].ToString();
                    _tracking.TrackingType = _tracking.PartnerTrackingType = _dRow["TrackingType"].ToString();
                    _tracking.TrackingNoteId = _dRow["TrackingNoteId"].ToString();
                    _tracking.PostClientHistoryStat = _dRow["PostStat"].ToString();
                    _tracking.SourceRefNo = _dRow["SourceRefNo"].ToString();
                    _tracking.BranchName = _dRow["Cabang"].ToString();
                    _tracking.VolDim = _dRow["VolDim"].ToString();
                    _tracking.ShippingFee = !string.IsNullOrEmpty(_dRow["ShippingFee"].ToString()) ? Convert.ToDecimal(_dRow["ShippingFee"]) : 0;
                    _tracking.InsFee = !string.IsNullOrEmpty(_dRow["InsFee"].ToString()) ? Convert.ToDecimal(_dRow["InsFee"]) : 0;
                    _tracking.PackingFee = !string.IsNullOrEmpty(_dRow["PackingFee"].ToString()) ? Convert.ToDecimal(_dRow["PackingFee"]) : 0;
                    _tracking.Weight = !string.IsNullOrEmpty(_dRow["Weight"].ToString()) ? Convert.ToDecimal(_dRow["Weight"]) : 0;
                    _tracking.IsPosted = !string.IsNullOrEmpty(_tracking.PostClientHistoryId) ? true : false;
                    _tracking.BranchCity = _dRow["BranchCity"].ToString();
                    _tracking.DestCode = _dRow["DestCode"].ToString();
                    //_tracking.EstShippingFee = 0; //not exist yet
                    //_tracking.EstWeight = !string.IsNullOrEmpty(_dRow["EstWeight"].ToString()) ? Convert.ToInt32(_dRow["EstWeight"]) : 0; ;
                    _tracking.OriginDestCode = _dRow["OriginDestCode"].ToString();
                    //_tracking.RecipientAddr = _dRow["RecipientAddr"].ToString();
                    _tracking.RecipientName = _dRow["RecipientName"].ToString();
                    if (!string.IsNullOrEmpty(_dRow["ResiInputDt"].ToString()))
                        _tracking.ResiInputDt = Convert.ToDateTime(_dRow["ResiInputDt"]);
                    //_tracking.SenderAddr = _dRow["SenderAddr"].ToString();
                    _tracking.SenderName = _dRow["SenderName"].ToString();
                    _tracking.ServiceType = _dRow["ServiceType"].ToString();
                    ResiTrackings.ListOfResiTracking.Add(_tracking);
                }

                ResiTrackings.GroupResiTrackingByReceiptNumber();

                #region set Additional Info
                var _addInfo = _type.GetMethod("SetAdditionalInfo");
                string _jsonSerialized = JsonConvert.SerializeObject(ResiTrackings.ListOfReceiptNumbers, Formatting.Indented);
                object[] parametersArray = new object[] { _jsonSerialized };
                object classInstance = Activator.CreateInstance(_type, null);
                _addInfo.Invoke(classInstance, parametersArray);

                #endregion

                foreach (var _receiptNumber in ResiTrackings.ListOfReceiptNumbers)
                {
                    prepareAndPostData(_receiptNumber);
                }
            }
            catch (Exception e)
            {
                writeLog($"ERROR: {e.Message.ToString()}");
                //exitConsole();
            }

            writeLog($"Console Integration [{PartnerSetting.PartnerName}] ENDED at [{DateTime.Now}]");

            if (_isStoreToDataTable == TRUE_VAL)
            {
                DataTable _result = new DataTable();
                _result = PostClientHistories.GenerateTableResult();
            }
        }

        static void prepareAndPostData(string receiptNumber)
        {
            var _trackings = ResiTrackings.GetResiTrackings(receiptNumber);

            try
            {
                bool isFinalStatReached = false;
                if (ResiTrackings.IsFinalTrackingPosted(receiptNumber))
                    isFinalStatReached = true;

                foreach (var _tracking in _trackings)
                {
                    if (isValidReceipt(receiptNumber))
                    {
                        var _postStat = ResiTrackings.SetPartnerStat(_tracking);
                        if (string.IsNullOrEmpty(_tracking.PostClientHistoryId))
                        {
                            if (isFinalStatReached)
                                postToPartner(_tracking, PostStats.SKIP);
                            else
                            {
                                //var _postStat = ResiTrackings.SetPartnerStat(_tracking);
                                postToPartner(_tracking, _postStat);
                            }
                        }
                        else //duplicate trackingnoteid detected
                        {
                            //var _postStat = ResiTrackings.SetPartnerStat(_tracking);
                            if (_tracking.PostClientHistoryStat == PostClientHistory.PostStats.ERROR.ToString()) //resend the data if error occured
                            {
                                //Update 23/11/2018
                                DateTime _OneMonthAgo = DateTime.Now.AddMonths(-1);
                                if (_tracking.TrackingDatetime >= _OneMonthAgo)
                                {
                                    postToPartner(_tracking, _postStat);
                                }
                                else 
                                    postToPartner(_tracking, PostStats.SKIP);
                            }
                            else
                                postToPartner(_tracking, PostStats.SKIP);
                        }
                    }
                    else
                        postToPartner(_tracking, PostStats.INVALID); //invalid receipt to be sent to Partner
                }
                //eo loop tracking data
            }
            catch (Exception e)
            {
                writeLog($"EXCEPTION {receiptNumber}: {e.Message}"); //no need to stop all operation while any exception occured
                //exitConsole();
            }
        }

        #region secondary Method
        static void postToPartner(ResiTracking tracking, PostStats postStat = PostStats.SENT)
        {
            string _responseString = string.Empty;
            if (postStat != PostStats.SKIP && postStat != PostStats.INVALID)
            {
                string _jsonSerialized = JsonConvert.SerializeObject(tracking, Formatting.Indented);
                object[] parametersArray = new object[] { _jsonSerialized };
                object classInstance = Activator.CreateInstance(_type, null);
                _responseString = _methodInfo.Invoke(classInstance, parametersArray).ToString();

                #region Old Method
                //if (_isStoreToDataTable == FALSE_VAL)
                //{
                //var PostData = new Dictionary<string, string>();
                //var _custPackageId = PartnerSetting.PartnerAPIParams.Where(a => a.Param == Params.CustPackageId.ToString()).FirstOrDefault();
                //if (_custPackageId != null)
                //{
                //    if (_custPackageId.IsSent == CommonConstanta.TRUE_VAL)
                //        PostData[_custPackageId.PartnerParamName] = tracking.CustPackageId;
                //}

                //var _receiptNumber = PartnerSetting.PartnerAPIParams.Where(a => a.Param == Params.ReceiptNumber.ToString()).FirstOrDefault();
                //if (_receiptNumber != null)
                //{
                //    if (_receiptNumber.IsSent == CommonConstanta.TRUE_VAL)
                //        PostData[_receiptNumber.PartnerParamName] = tracking.ReceiptNumber;
                //}

                //var _partnerTrackingType = PartnerSetting.PartnerAPIParams.Where(a => a.Param == Params.PartnerTrackingType.ToString()).FirstOrDefault();
                //if (_partnerTrackingType != null)
                //{
                //    if (_partnerTrackingType.IsSent == CommonConstanta.TRUE_VAL)
                //        PostData[_partnerTrackingType.PartnerParamName] = tracking.PartnerTrackingType;
                //}

                //var _trackingNotes = PartnerSetting.PartnerAPIParams.Where(a => a.Param == Params.TrackingNotes.ToString()).FirstOrDefault();
                //if (_trackingNotes != null)
                //{
                //    if (_trackingNotes.IsSent == CommonConstanta.TRUE_VAL)
                //        PostData[_trackingNotes.PartnerParamName] = tracking.Notes;
                //}

                //var _trackingDatetime = PartnerSetting.PartnerAPIParams.Where(a => a.Param == Params.TrackingDatetime.ToString()).FirstOrDefault();
                //if (_trackingDatetime != null)
                //{
                //    if (_trackingDatetime.IsSent == CommonConstanta.TRUE_VAL)
                //        PostData[_trackingDatetime.PartnerParamName] = Convert.ToDateTime(tracking.TrackingDatetime).ToString("yyyy-MM-dd HH:mm:ss");
                //}

                //string url = PartnerSetting.APIURL;
                //var client = new HttpClient();
                //if (PartnerSetting.IsUseHeaderAuth == CommonConstanta.TRUE_VAL)
                //    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(PartnerSetting.HeaderAuthName, PartnerSetting.HeaderAuthKey);
                //var content = new FormUrlEncodedContent(PostData);
                //var response = client.PostAsync(url, content);
                //_responseString = response.Result.StatusCode.ToString();


                //} 
                #endregion
            }

            tracking.IsPosted = true;
            var _postClientHist = PostClientHistory.ConvertToPostClientHistory(tracking, _responseString, PartnerSetting.APIKey, postStat);
            saveTrackingHist(_postClientHist);
        }

        static bool saveTrackingHist(PostClientHistory postClientHistory)
        {
            if (_isStoreToDataTable == TRUE_VAL)
                PostClientHistories.AddPostClientHistory(postClientHistory); //debugging purpose
            else
            {
                string _query = string.Empty;
                if (postClientHistory.UpdateType == CRUDType.INSERT)
                {
                    _query = @"   INSERT INTO POD.DBO.[PostClientHistory] 
                                                (   [trackingNoteIdInt] ,
                                                    [UpdateDatetime] ,
                                                    [clientResponse], 
                                                    [TrackingType], 
                                                    UsrCrt,
                                                    UsrUpd,
                                                    DtmCrt, 
                                                    DtmUpd, 
                                                    checkContent, 
                                                    [APIKey], 
                                                    ReceiptNumber, 
                                                    SiCepatTrackingType,
                                                    PostStat,
                                                    SourceRefNo
                                                ) 
                                         VALUES(
                                                    " + postClientHistory.TrackingNoteIdInt + ", " +
                                                            "'" + postClientHistory.UpdateDatetime + "', " +
                                                            "'" + postClientHistory.ClientResponse + "'," +
                                                            "'" + postClientHistory.TrackingType + "'," +
                                                            "'IntConsole'," +
                                                            "'IntConsole'," +
                                                            "'" + DateTime.Now + "'," +
                                                            "'" + DateTime.Now + "'," +
                                                            "'" + postClientHistory.CheckContent + "'," +
                                                            "'" + postClientHistory.APIKey + "'," +
                                                            "'" + postClientHistory.ReceiptNumber + "'," +
                                                            "'" + postClientHistory.SiCepatTrackingType + "'," +
                                                            "'" + postClientHistory.PostStat + "'," +
                                                            "'" + postClientHistory.SourceRefNo + "'" +
                                                        ")";
                }
                else
                {
                    _query = @"   UPDATE POD.DBO.[PostClientHistory] 
                              SET   clientResponse = '" + postClientHistory.ClientResponse + "'," +
                                        "UsrUpd = 'IntConsole'," +
                                        "DtmUpd = '" + DateTime.Now + "'," +
                                        "PostStat = '" + postClientHistory.PostStat + "'" +
                                  " WHERE Id = " + postClientHistory.PostClientHistoryId;
                }

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SiCepatConn"].ConnectionString);
                try
                {
                    conn.Open();
                    SqlCommand cmd2 = new SqlCommand(_query, conn);
                    var rd2d = cmd2.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    writeLog(_query + " error " + e.Message.ToString());
                    exitConsole();
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return true;
        }

        static bool isValidReceipt(string receiptNumber)
        {
            if (PartnerSetting.ListOfValidResi.Count() > 0)
            {
                foreach (var _validReceiptRange in PartnerSetting.ListOfValidResi)
                {
                    long _startRange = _validReceiptRange.Key;
                    long _endRange = _validReceiptRange.Value;

                    long _convertedReceiptNumber = Convert.ToInt64(receiptNumber);
                    if (_convertedReceiptNumber >= _startRange && _convertedReceiptNumber <= _endRange)
                        return true;
                }
            }
            else
                return true; //if allowed resi list null then no validation at all 

            return false;
        }

        static void writeLog(string str)
        {
            string _logPath = ConfigurationManager.AppSettings["LogPath"];
            using (StreamWriter w = File.AppendText(_logPath))
            {
                var date = DateTime.Now.ToString();
                w.WriteLine(date + " ==> " + str);
            }
        }
        #endregion

        #region Supporting Methods
        static void Main(string[] args)
        {
            //args = new string[2];
            //args[0] = ExecMethod.SPECIFIC_STT.ToString();
            //args[1] = "000152924414";

            //args = new string[3];
            //args[0] = ExecMethod.DEFINED_PERIOD.ToString();
            //args[1] = "6/23/2018";
            //args[2] = "6/24/2018";

            string appName = ConfigurationManager.AppSettings["ConsoleName"];
            bool createdNew;

            mutex = new Mutex(true, appName, out createdNew);

            if (!createdNew)
            {
                writeLog(appName + " is already running! Exiting the application.");
                Environment.Exit(0);
            }
            else
            {
                IntegrationMain(args);
            }

        }

        static void exitConsole()
        {
            writeLog("Due to previous DB connection error, console App is stopped to allow other instance to run.");
            Environment.Exit(0);
        }

        #endregion

        #region data Retriever
        static DataTable getOutstandingTrackingData(string[] args)
        {
            DataTable _dataToBePost = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SiCepatConn"].ConnectionString);
            try
            {
                string _addCondition = buildConditionBasedOnArgs(args);
                //string _cmdString = @"  select      pr.APIKey,
                //                                    pr.ReceiptNumber,
                //                                    pr.TrackingDatetime,
                //                                    pr.TrackingType,
                //                                    pr.TrackingNoteId,
                //                                    pr.Id as sourceRefNo,
                //                                    pr.cust_package_id,
                //                                    pr.Cabang,
                //                                    pr.note,
                //                                    pch.id,
                //                                    pch.PostStat
                //                        from        partnerresi pr (nolock)
                //                        left join   PostClientHistory pch (nolock)
                //                        on          pr.id = pch.SourceRefNo
                //                        where       pr.ReceiptNumber in
                //                        (
                //                            select      pr.ReceiptNumber
                //                            from        partnerresi pr (nolock)
                //                            left join   PostClientHistory pch (nolock)
                //                            on          pr.id = pch.SourceRefNo 
                //                            where       (pch.id is null or pch.PostStat = 'ERROR') and pr.name = '" + PartnerSetting.PartnerName + "' " + _addCondition +
                //                       ")";

                //string _cmdString = @"  select      pr.APIKey,
                //                                    pr.ReceiptNumber,
                //                                    pr.TrackingDatetime,
                //                                    pr.TrackingType,
                //                                    pr.TrackingNoteId,
                //                                    pr.Id as sourceRefNo,
                //                                    pr.cust_package_id,
                //                                    pr.Cabang,
                //                                    pr.note,
                //                                    pch.id,
                //                                    pch.PostStat,
                //                                    stt.berat as weight,
                //                                    stt.totalbiaya as shippingfee,
                //                                    stt.asuransibiaya as insFee,
                //                                    stt.packingbiaya as packingFee,
                //                                    stt.harga as pricePerKg,
                //                                    stt.ketvolume as volDim,
                //                           stt.layanan as serviceType,
                //                           stt.pengirim as SenderName, 
                //                           stt.penerima as RecipientName,
                //                           stt.asal as OriginDestCode,
                //                           stt.tujuan as DestCode,
                //                           stt.tglInput as ResiInputDt,
                //                           ts.city as branchcity
                //                        from        partnerresi pr (nolock)
                //                        left join   PostClientHistory pch (nolock)
                //                        on          pr.id = pch.SourceRefNo
                //                        left join   stt stt (nolock)
                //                        on          pr.receiptnumber = stt.nostt
                //                        left join	mstrackingsite ts (nolock)
                //                        on			pr.sitecode = ts.sitecode
                //                        where       pr.ReceiptNumber in
                //                        (
                //                            select      pr.ReceiptNumber
                //                            from        partnerresi pr (nolock)
                //                            left join   PostClientHistory pch (nolock)
                //                            on          pr.id = pch.SourceRefNo 
                //                            where       (pch.id is null or pch.PostStat = 'ERROR') and pr.name = '" + PartnerSetting.PartnerName + "' " + _addCondition +
                //                       ")";

                string _cmdString = @"
                    select pr.APIKey,
                        pr.ReceiptNumber,
                        pr.TrackingDatetime,
                        pr.TrackingType,
                        pr.TrackingNoteId,
                        pr.Id as sourceRefNo,
                        pr.cust_package_id,
                        pr.Cabang,
                        pr.note,
                        pch.id,
                        pch.PostStat,
                        stt.berat as weight,
                        stt.totalbiaya as shippingfee,
                        stt.asuransibiaya as insFee,
                        stt.packingbiaya as packingFee,
                        stt.harga as pricePerKg,
                        stt.ketvolume as volDim,
                        stt.layanan as serviceType,
                        stt.pengirim as SenderName, 
                        stt.penerima as RecipientName,
                        stt.asal as OriginDestCode,
                        stt.tujuan as DestCode,
                        stt.tglInput as ResiInputDt,
                        ts.city as branchcity													
                    from partnerresi pr with (nolock)
                    left join PostClientHistory pch with (nolock) on pr.id = pch.SourceRefNo
                    left join stt stt with (nolock) on pr.receiptnumber = stt.nostt
                    left join mstrackingsite ts with (nolock) on pr.sitecode = ts.sitecode
                    inner join UDF_PostClientHistoryErwin('"+ PartnerSetting.PartnerName + "', " + _addCondition + ") x on x.receiptnumber=pr.receiptnumber";

                SqlCommand cmd = new SqlCommand(_cmdString, conn);
                cmd.CommandTimeout = 420;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(_dataToBePost);
            }
            catch (Exception e)
            {
                writeLog($"SQL Error => {e.Message.ToString()}");
                exitConsole();
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
            return _dataToBePost;
        }

        static string buildConditionBasedOnArgs(string[] args)
        {
            string _period = ConfigurationManager.AppSettings["DefaultRangePeriodInHour"];
            string _cond = $"NULL, NULL, NULL"; //default 
            if (args.Count() > 0)
            {
                if (args[0].ToString().Equals(ExecMethod.DEFINED_PERIOD.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    DateTime _startDt;
                    DateTime _endDt;
                    //period should have 3 params
                    if (args.Count() == 3)
                    {
                        _execMethod = ExecMethod.DEFINED_PERIOD;
                        if (DateTime.TryParse(args[1].ToString(), out _startDt) && DateTime.TryParse(args[2].ToString(), out _endDt))
                        {
                            _startDt = _startDt.Date;
                            _endDt = _endDt.Date;
                            _cond = $"'{args[1]}', '{args[2]}', NULL";
                        }
                    }
                    else if (args.Count() == 2)
                    {
                        _execMethod = ExecMethod.SPECIFIC_DATE;
                        if (DateTime.TryParse(args[1].ToString(), out _startDt))
                        {
                            _startDt = _startDt.Date;
                            _endDt = _startDt.AddDays(1).Date; //one day period
                            _cond = $"'{args[1]}', '{args[2]}', NULL";
                        }
                    }
                }
                else if (args[0].ToString().Equals(ExecMethod.SPECIFIC_STT.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    if (args.Count() == 2)
                    {
                        if (args[1].ToString().Count() == 12)
                        {
                            _execMethod = ExecMethod.SPECIFIC_STT;
                            _cond = $"NULL, NULL, '{args[1]}'";
                        }
                    }
                }
            }

            return _cond;
        }

        //static string buildConditionBasedOnArgs(string[] args)
        //{
        //    string _period = ConfigurationManager.AppSettings["DefaultRangePeriodInHour"];
        //    string _cond = $" and pr.dtmupd >= DATEADD(hour, {_period}, SWITCHOFFSET(SYSUTCDATETIME(), '+07:00'))"; //default 
        //    if (args.Count() > 0)
        //    {
        //        if (args[0].ToString().Equals(ExecMethod.DEFINED_PERIOD.ToString(), StringComparison.OrdinalIgnoreCase))
        //        {
        //            DateTime _startDt;
        //            DateTime _endDt;
        //            //period should have 3 params
        //            if (args.Count() == 3)
        //            {
        //                _execMethod = ExecMethod.DEFINED_PERIOD;
        //                if (DateTime.TryParse(args[1].ToString(), out _startDt) && DateTime.TryParse(args[2].ToString(), out _endDt))
        //                {
        //                    _startDt = _startDt.Date;
        //                    _endDt = _endDt.Date;
        //                    _cond = $" and pr.dtmupd >= '{args[1]}' and pr.dtmupd < '{args[2]}'";
        //                }
        //            }
        //            else if (args.Count() == 2)
        //            {
        //                _execMethod = ExecMethod.SPECIFIC_DATE;
        //                if (DateTime.TryParse(args[1].ToString(), out _startDt))
        //                {
        //                    _startDt = _startDt.Date;
        //                    _endDt = _startDt.AddDays(1).Date; //one day period
        //                    _cond = $" and pr.dtmupd >= '{args[1]}' and pr.dtmupd < '{args[2]}'";
        //                }
        //            }
        //        }
        //        else if (args[0].ToString().Equals(ExecMethod.SPECIFIC_STT.ToString(), StringComparison.OrdinalIgnoreCase))
        //        {
        //            if (args.Count() == 2)
        //            {
        //                if (args[1].ToString().Count() == 12)
        //                {
        //                    _execMethod = ExecMethod.SPECIFIC_STT;
        //                    _cond = $" and pr.ReceiptNumber = '{args[1]}'";
        //                }
        //            }
        //        }
        //    }

        //    return _cond;
        //}
        #endregion
    }
}
