﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.TrackingScheduler._customObj
{
    public class CommonConstanta
    {
        public enum CRUDType
        {
            INSERT,
            UPDATE
        }

        public static string TRUE_VAL = "1";
        public static string FALSE_VAL = "0";
    }
}
