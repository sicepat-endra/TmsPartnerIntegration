﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static TMS.TrackingScheduler._customObj.CommonConstanta;

namespace TMS.TrackingScheduler._customObj
{
    public class PostClientHistory
    {
        public enum PostStats
        {
            SENT,
            ERROR,
            SKIP,
            INVALID
        }        

        public string TrackingNoteId { get; set; }
        public DateTime UpdateDatetime { get; set; }
        public string ClientResponse { get; set; }
        public string TrackingType { get; set; }
        public DateTime DtmCrt { get; set; }
        public string UsrCrt { get; set; }
        public DateTime DtmUpd { get; set; }
        public string UsrUpd { get; set; }
        public string CheckContent { get; set; }
        public Int64 TrackingNoteIdInt { get; set; }
        public string APIKey { get; set; }
        public string ReceiptNumber { get; set; }
        public string SiCepatTrackingType { get; set; }
        public Nullable<Int64> PostClientHistoryId { get; set; }
        public PostStats PostStat { get; set; }
        public CRUDType UpdateType { get; set; }
        public string SourceRefNo { get; set; }

        public static PostClientHistory ConvertToPostClientHistory(ResiTracking tracking, string postResult, string apiKey, PostStats postStat = PostStats.SENT, CRUDType commandType = CRUDType.INSERT)
        {
            string[] _successResponses = { "Accepted", "DoNotPostThis", "OK", "success" };
            string _log = $"CustPckgId = {tracking.CustPackageId}, resi={tracking.ReceiptNumber}, Tr_Type={tracking.TrackingType}, Notes={tracking.Notes.Replace("'", "")}, trackingDatetime={tracking.TrackingDatetime}";

            PostClientHistory _postClientHistory = new PostClientHistory();
            _postClientHistory.TrackingNoteIdInt = 0;
            _postClientHistory.UpdateDatetime = tracking.TrackingDatetime;
            _postClientHistory.TrackingType = tracking.PartnerTrackingType;
            _postClientHistory.ClientResponse = postResult;
            _postClientHistory.CheckContent = _log;
            _postClientHistory.APIKey = apiKey;
            _postClientHistory.ReceiptNumber = tracking.ReceiptNumber;
            _postClientHistory.SiCepatTrackingType = tracking.TrackingType;
            _postClientHistory.PostClientHistoryId = !string.IsNullOrEmpty(tracking.PostClientHistoryId) ? Convert.ToInt64(tracking.PostClientHistoryId) : 0;
            _postClientHistory.SourceRefNo = tracking.SourceRefNo;
            _postClientHistory.PostStat = _successResponses.Contains(postResult) ? PostStats.SENT : postStat == PostStats.SKIP ? PostStats.SKIP : !_successResponses.Contains(postResult) ? PostStats.ERROR : postStat;
            _postClientHistory.UpdateType = !string.IsNullOrEmpty(tracking.PostClientHistoryId) ? CRUDType.UPDATE : CRUDType.INSERT;
            _postClientHistory.UsrUpd = _postClientHistory.UsrCrt = "NEWBATCH";
            _postClientHistory.DtmUpd = _postClientHistory.DtmCrt = DateTime.Now;
            return _postClientHistory;
        }
    }

    public class PostClientHistories
    {
        public static List<PostClientHistory> ListOfPostClientHistory = new List<PostClientHistory>();

        public static DataTable PostClientHistoryTbl = new DataTable();

        public static bool AddPostClientHistory(PostClientHistory postClientHistory)
        {
            //PostClientHistory _postClientHistory = new PostClientHistory();
            //_postClientHistory.TrackingNoteIdInt = trackingNoteIdInt;
            //_postClientHistory.UpdateDatetime = updatedatetime;
            //_postClientHistory.TrackingType = trackingType;
            //_postClientHistory.DtmCrt = DateTime.Now;
            //_postClientHistory.UsrCrt = usrCrt;
            //_postClientHistory.DtmUpd = DateTime.Now;
            //_postClientHistory.UsrUpd = usrupd;
            //_postClientHistory.CheckContent = checkContent;
            ////_postClientHistory.TrackingNoteId = trackingNoteId;
            //_postClientHistory.APIKey = apiKey;
            //_postClientHistory.ReceiptNumber = receiptNumber;
            //_postClientHistory.SiCepatTrackingType = sicepatTracking;

            ListOfPostClientHistory.Add(postClientHistory);
            return true;
        }

        public static DataTable GenerateTableResult()
        {
            PostClientHistoryTbl.Columns.Add("SourceRefNo");
            PostClientHistoryTbl.Columns.Add("UpdateDatetime");
            PostClientHistoryTbl.Columns.Add("TrackingType");
            PostClientHistoryTbl.Columns.Add("DtmCrt");
            PostClientHistoryTbl.Columns.Add("UsrCrt");
            PostClientHistoryTbl.Columns.Add("DtmUpd");
            PostClientHistoryTbl.Columns.Add("UsrUpd");
            PostClientHistoryTbl.Columns.Add("CheckContent");
            PostClientHistoryTbl.Columns.Add("APIKey");
            PostClientHistoryTbl.Columns.Add("ReceiptNumber");
            PostClientHistoryTbl.Columns.Add("SiCepatTracking");
            PostClientHistoryTbl.Columns.Add("PostStat");
            PostClientHistoryTbl.Columns.Add("UpdateType");

            foreach (var _hist in ListOfPostClientHistory)
            {
                DataRow _dRow = PostClientHistoryTbl.NewRow();
                _dRow["SourceRefNo"] = _hist.SourceRefNo;
                _dRow["UpdateDatetime"] = _hist.UpdateDatetime;
                _dRow["TrackingType"] = _hist.TrackingType;
                _dRow["DtmCrt"] = _hist.DtmCrt;
                _dRow["UsrCrt"] = _hist.UsrCrt;
                _dRow["DtmUpd"] = _hist.DtmUpd;
                _dRow["UsrUpd"] = _hist.UsrUpd;
                _dRow["CheckContent"] = _hist.CheckContent;
                _dRow["APIKey"] = _hist.APIKey;
                _dRow["ReceiptNumber"] = _hist.ReceiptNumber;
                _dRow["SiCepatTracking"] = _hist.SiCepatTrackingType;
                _dRow["PostStat"] = _hist.PostStat;
                _dRow["UpdateType"] = _hist.UpdateType;

                PostClientHistoryTbl.Rows.Add(_dRow);
            }

            return PostClientHistoryTbl;
        }
    }
}

