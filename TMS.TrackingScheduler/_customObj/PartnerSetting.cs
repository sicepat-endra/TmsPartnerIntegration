﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using static TMS.TrackingScheduler._customObj.ResiTracking;

namespace TMS.TrackingScheduler._customObj
{
    public class StatusMapper
    {
        public string InternalStat { get; set; }
        public string PartnerStat { get; set; }
        public string IsSkipped { get; set; }
        public int MaxPost { get; set; }
        public string IsProblem { get; set; }
        public string IsAddSeqInfo { get; set; }
    }

    //public class APIParam
    //{
    //    public enum Params
    //    {
    //        CustPackageId,
    //        ReceiptNumber,
    //        PartnerTrackingType,
    //        TrackingNotes,
    //        TrackingDatetime
    //    }

    //    public string Param { get; set; }
    //    public string PartnerParamName { get; set; }
    //    public string IsSent { get; set; }
    //}

    public class PartnerSetting
    {
        public static string PartnerName { get; set; }
        public static string APIURL { get; set; }
        public static string APIKey { get; set; }
        public static string IsUseHeaderAuth { get; set; }
        public static string HeaderAuthKey { get; set; }
        public static string HeaderAuthName { get; set; }
        public static string PostAssembly { get; set; }
        public static List<StatusMapper> ListOfStatusMapper = new List<StatusMapper>();
        public static Dictionary<long, long> ListOfValidResi = new Dictionary<long, long>();

        public static void SetPartnerConfiguration()
        {
            XElement xelement = XElement.Load("PartnerSetting.xml");
            #region Tracking Data Preparation
            IEnumerable<XElement> _statuses = xelement.Elements("Statuses");
            foreach (var _status in _statuses.Elements())
            {
                StatusMapper _mapper = new StatusMapper();
                _mapper.InternalStat = _status.Element("InternalStat").Value;
                _mapper.PartnerStat = _status.Element("PartnerStat").Value;
                _mapper.IsSkipped = _status.Element("IsSkipped").Value;
                _mapper.IsProblem = _status.Element("IsProblemStat").Value;
                _mapper.MaxPost = Convert.ToInt32(_status.Element("MaxPost").Value);
                _mapper.IsAddSeqInfo = _status.Element("IsAddSeqInfo").Value;
                ListOfStatusMapper.Add(_mapper);

                var _isFinalPost = _status.Element("IsFinalPost").Value;
                if (_isFinalPost == CommonConstanta.TRUE_VAL)
                    ResiTracking.ListOfFinalStats.Add(_mapper.InternalStat);

                var _isPOD = _status.Element("IsPOD").Value;
                if (_isPOD == CommonConstanta.TRUE_VAL)
                    ResiTracking.ListOfPODStats.Add(_mapper.InternalStat);
            }
            #endregion

            #region Allowed Resi Binding
            IEnumerable<XElement> _allowedReceipts = xelement.Elements("AllowedReceipts");
            foreach (var _receipts in _allowedReceipts.Elements())
            {
                ListOfValidResi.Add(Convert.ToInt64(_receipts.Element("startNum").Value), Convert.ToInt64(_receipts.Element("endNum").Value));
            }
            #endregion

            PartnerName = xelement.Elements("PartnerName").FirstOrDefault().Value;
            var _api = xelement.Elements("API").FirstOrDefault();
            IsUseHeaderAuth = _api.Elements("IsUseHeaderAuth").FirstOrDefault().Value;
            HeaderAuthKey = _api.Elements("HeaderAuthKey").FirstOrDefault().Value;
            HeaderAuthName = _api.Elements("HeaderAuthName").FirstOrDefault().Value;
            APIKey = _api.Elements("Key").FirstOrDefault().Value;
            APIURL = _api.Elements("URL").FirstOrDefault().Value;
            PostAssembly = _api.Elements("PostAssembly").FirstOrDefault().Value;
        }

        public static StatusMapper GetStatusMapper(ResiTracking tracking)
        {
            string _trackType = tracking.TrackingType;
            switch (tracking.TrackingType)
            {
                case "OUT":
                    List<string> _statusesForOut = new List<string>();
                    _statusesForOut.AddRange(ResiTracking.ListOfPODStats);
                    _statusesForOut.Add(TrackingTypes.RTA.ToString());
                    _statusesForOut.Add(TrackingTypes.RTN.ToString());

                    var _availableStatsForOut = ResiTrackings.GetResiTrackings(tracking.ReceiptNumber, _statusesForOut, true);
                    if (_availableStatsForOut != null)
                    {
                        var _lastStat = _availableStatsForOut.Where(a => a.TrackingDatetime <= tracking.TrackingDatetime).OrderByDescending(a => a.TrackingDatetime).FirstOrDefault();
                        if (_lastStat != null)
                        {
                            if (_lastStat.TrackingType == TrackingTypes.RTN.ToString())
                                _trackType = TrackingTypes.OUT_RTN.ToString();
                            else if (_lastStat.TrackingType == TrackingTypes.RTA.ToString())
                                _trackType = TrackingTypes.OUT_RTR.ToString();
                            else if (ResiTracking.ListOfPODStats.Contains(_lastStat.TrackingType))
                                _trackType = TrackingTypes.OUT_RDLV.ToString();
                            else if (tracking.SameStatSeqNo > 1)
                                _trackType = TrackingTypes.DPS_OUT.ToString();
                        }
                        else if (tracking.SameStatSeqNo > 1)
                            _trackType = TrackingTypes.DPS_OUT.ToString();
                    }
                    else if (tracking.SameStatSeqNo > 1)
                        _trackType = TrackingTypes.DPS_OUT.ToString();

                    #region old method
                    //if (ResiTrackings.IsTrackingTypeExist(tracking.ReceiptNumber, TrackingTypes.RTN, true))
                    //    _trackType = TrackingTypes.OUT_RTN.ToString();
                    //else if (ResiTrackings.IsTrackingTypeExist(tracking.ReceiptNumber, TrackingTypes.RTA, true))
                    //    _trackType = TrackingTypes.OUT_RTR.ToString();
                    //else if (ResiTrackings.IsTrackingTypeExist(tracking.ReceiptNumber, ResiTracking.ListOfPODStats, true))
                    //    _trackType = TrackingTypes.OUT_RDLV.ToString();
                    //else if (tracking.SameStatSeqNo > 1)
                    //    _trackType = TrackingTypes.DPS_OUT.ToString(); 
                    #endregion
                    break;
                case "IN":
                    var _firstOutTrack = ResiTrackings.GetFirstTrackingType(tracking.ReceiptNumber, TrackingTypes.OUT, true);
                    if (_firstOutTrack != null)
                        if (tracking.SameStatSeqNo > 1 || _firstOutTrack.BranchName != tracking.BranchName)
                            _trackType = TrackingTypes.DPS_IN.ToString();
                    break;
                case "ANT":
                    List<string> _statusesForAnt = new List<string>();
                    _statusesForAnt.AddRange(ResiTracking.ListOfPODStats);
                    _statusesForAnt.Add(TrackingTypes.RTA.ToString());
                    _statusesForAnt.Add(TrackingTypes.RTN.ToString());

                    var _availableStatsForAnt = ResiTrackings.GetResiTrackings(tracking.ReceiptNumber, _statusesForAnt, true);
                    if (_availableStatsForAnt != null)
                    {
                        var _lastStat = _availableStatsForAnt.Where(a => a.TrackingDatetime <= tracking.TrackingDatetime).OrderByDescending(a => a.TrackingDatetime).FirstOrDefault();
                        if (_lastStat != null)
                        {
                            if (_lastStat.TrackingType == TrackingTypes.RTN.ToString())
                                _trackType = TrackingTypes.ANT_RTN.ToString();
                            else if (_lastStat.TrackingType == TrackingTypes.RTA.ToString())
                                _trackType = TrackingTypes.ANT_RTR.ToString();
                            else if (ResiTracking.ListOfPODStats.Contains(_lastStat.TrackingType))
                                _trackType = TrackingTypes.ANT_RDLV.ToString();
                        }
                    }

                    #region old method
                    //if (ResiTrackings.IsTrackingTypeExist(tracking.ReceiptNumber, TrackingTypes.RTN, true))
                    //    _trackType = TrackingTypes.ANT_RTN.ToString();
                    //else if (ResiTrackings.IsTrackingTypeExist(tracking.ReceiptNumber, TrackingTypes.RTA, true))
                    //    _trackType = TrackingTypes.ANT_RTR.ToString();
                    //else if (ResiTrackings.IsTrackingTypeExist(tracking.ReceiptNumber, ResiTracking.ListOfPODStats, true))
                    //    _trackType = TrackingTypes.ANT_RDLV.ToString(); 
                    #endregion
                    break;
            }


            var _statAttr = tracking.SameStatSeqNo > 1 ? $"_{tracking.SameStatSeqNo}" : string.Empty;
            var _trackingType = _trackType + _statAttr;

            var _mapperResult = PartnerSetting.ListOfStatusMapper.Where(a => a.InternalStat == _trackingType).FirstOrDefault();
            if (_mapperResult == null)
            {
                _trackingType = _trackType + "_X";
                _mapperResult = PartnerSetting.ListOfStatusMapper.Where(a => a.InternalStat == _trackingType).FirstOrDefault();
                if (_mapperResult == null)
                {
                    _trackingType = _trackType;
                    _mapperResult = PartnerSetting.ListOfStatusMapper.Where(a => a.InternalStat == _trackingType).FirstOrDefault();
                }
            }

            return _mapperResult;
        }

    }
}
