﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static TMS.TrackingScheduler._customObj.CommonConstanta;

namespace TMS.TrackingScheduler._customObj
{
    public class SttSummary
    {
        public enum FinalDeliveryDecisionOpt
        {
            DLV,    //deliver to cust
            RTN,    //return to shipper
            RPL     //replacement
        }

        public SttSummary()
        {
            DeliveryAttemptCount = 0; //default value (hasn't do any delivery process to cust yet)
            FinalDeliveryDecision = FinalDeliveryDecisionOpt.DLV.ToString(); //default value for every delivery process
        }
        public string ReceiptNumber { get; set; }
        public int DeliveryAttemptCount { get; set; }
        public DateTime LastTrackingDatetime { get; set; }
        public string LastTrackingType { get; set; }
        public DateTime LastProblemDatetime { get; set; }
        public string LastProblemStat { get; set; }
        public string FinalDeliveryDecision { get; set; }
        public string CRUDFlag { get; set; }
    }

    public class SttSummaries
    {
        public static List<SttSummary> ListOfSttSummary { get; set; } = new List<SttSummary>();

        public static SttSummary GetSttSummary(string receiptNumber)
        {
            var _sttSummary = ListOfSttSummary.Where(a => a.ReceiptNumber == receiptNumber).FirstOrDefault();
            return _sttSummary;
        }

        public static bool AddSttSummary(SttSummary sttSummary)
        {
            if (string.IsNullOrEmpty(sttSummary.LastTrackingType))
                sttSummary.CRUDFlag = CRUDType.INSERT.ToString();
            else
                sttSummary.CRUDFlag = CRUDType.UPDATE.ToString();
            return true;
        }
    }
}
