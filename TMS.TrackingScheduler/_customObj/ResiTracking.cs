﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using static TMS.TrackingScheduler._customObj.PostClientHistory;

namespace TMS.TrackingScheduler._customObj
{
    public class ResiTracking
    {
        public enum TrackingTypes
        {
            DLV,
            PICKREQ,
            IN,
            OUT,
            OUT_RDLV,
            OUT_RTN,
            OUT_RTR,
            PICK,
            ANT,
            ANT_RTN,
            ANT_RTR,
            ANT_RDLV,
            DPS_OUT,
            DPS_IN,
            RTS,
            RTA,
            RTN
        }

        public static List<string> ListOfFinalStats = new List<string>();
        public static List<string> ListOfPODStats = new List<string>();

        public string ReceiptNumber { get; set; }
        public DateTime TrackingDatetime { get; set; }
        public string TrackingType { get; set; }
        public string TrackingNoteId { get; set; }
        public string BranchName { get; set; }
        public string CustPackageId { get; set; }
        public string Notes { get; set; }
        public string PostClientHistoryId { get; set; }
        public string PostClientHistoryStat { get; set; }
        public string PartnerTrackingType { get; set; }
        public string SourceRefNo { get; set; } //PartnerResi.Id
        public decimal Weight { get; set; } = 0;
        public decimal ShippingFee { get; set; } = 0;
        public decimal InsFee { get; set; } = 0;
        public decimal PackingFee { get; set; } = 0;
        public decimal PricePerKg { get; set; } = 0;
        public DateTime? ResiInputDt { get; set; }
        //public int EstWeight { get; set; } = 0;
        //public int EstShippingFee { get; set; } = 0;
        public string OriginDestCode { get; set; }
        public string DestCode { get; set; }
        public string ServiceType { get; set; }
        public string RecipientName { get; set; }
        public string SenderName { get; set; }
        //public string RecipientAddr { get; set; }
        //public string SenderAddr { get; set; }
        public string BranchCity { get; set; }
        public string VolDim
        {
            get { return _voldim; } set
            {
                if (value != null)
                {
                    var _val = value.Split('x');
                    if (_val.Length == 3)
                    {
                        Length = Convert.ToDecimal(_val[0]);
                        Width = Convert.ToDecimal(_val[1]);
                        Height = Convert.ToDecimal(_val[2]);
                    }
                    else
                        _voldim = value;
                }
                else
                    _voldim = "0x0x0";
            }
        }
        private string _voldim { get; set; }
        public decimal Height { get; set; } = 0;
        public decimal Width { get; set; } = 0;
        public decimal Length { get; set; } = 0;
        public static bool IsFinalStat(string trackingType)
        {
            if (ListOfFinalStats.Contains(trackingType))
                return true;
            else
                return false;
        }
        public bool IsPosted { get; set; } = false;
        public int SameStatSeqNo { get; set; } = 0;
    }

    public class ResiTrackings
    {
        public static List<ResiTracking> ListOfResiTracking { get; set; } = new List<ResiTracking>();
        public static List<string> ListOfReceiptNumbers = new List<string>();
        public static DataTable AdditionalInfos = new DataTable();

        public static void GroupResiTrackingByReceiptNumber()
        {
            var _list = (from a in ListOfResiTracking
                         group a by a.ReceiptNumber into grouped
                         select new { ReceiptNumber = grouped.Key, FirstItem = grouped.FirstOrDefault() });

            ListOfReceiptNumbers = _list.Select(a => a.ReceiptNumber).ToList();
        }

        public static List<ResiTracking> GetResiTrackings(string receiptNumber)
        {
            var _list = ListOfResiTracking.Where(a => a.ReceiptNumber == receiptNumber)
                                            .OrderBy(x => x.TrackingDatetime); //.ToList();

            List<ResiTracking> _listOfTrack = new List<ResiTracking>();
            foreach (var _tracking in _list)
            {
                var _prevSameStat = _list.Where(a => a.TrackingType == _tracking.TrackingType && a.TrackingDatetime < _tracking.TrackingDatetime).OrderBy(a => a.TrackingDatetime);
                var _lastSameStat = _prevSameStat.LastOrDefault();
                if (_lastSameStat != null)
                {
                    //if (_lastNotePublic != _tracking.Notes && _lastTrackingType != _tracking.TrackingType)
                    if (_lastSameStat.BranchName != _tracking.BranchName)
                    {
                        var _seqNo = _list.Where(a => a.TrackingType == _tracking.TrackingType).Max(a => a.SameStatSeqNo) + 1;
                        _tracking.SameStatSeqNo = _seqNo;

                        if (string.IsNullOrEmpty(_tracking.PostClientHistoryId) || _tracking.PostClientHistoryStat == PostClientHistory.PostStats.ERROR.ToString())
                            _listOfTrack.Add(_tracking);
                    }
                }
                else
                {
                    _tracking.SameStatSeqNo = 1;
                    if (string.IsNullOrEmpty(_tracking.PostClientHistoryId) || _tracking.PostClientHistoryStat == PostClientHistory.PostStats.ERROR.ToString())
                        _listOfTrack.Add(_tracking);

                }
            }

            return _listOfTrack;
        }

        public static bool IsFinalTrackingPosted(string receiptNumber)
        {
            return getPostedResiTrackings(receiptNumber, true).Where(a => ResiTracking.ListOfFinalStats.Contains(a.TrackingType)).Any();
            //return getPostedResiTrackings(receiptNumber).Where(a => ResiTracking.ArrayFinalStatus.Contains(a.TrackingType) && a.PostClientHistoryId != null).Any();
        }

        public static ResiTracking GetFirstTrackingType(string receiptNumber, ResiTracking.TrackingTypes trackingType, bool isJustPostedIncluded = false)
        {
            return getPostedResiTrackings(receiptNumber, isJustPostedIncluded).Where(a => a.TrackingType == trackingType.ToString()).OrderBy(a => a.TrackingDatetime).FirstOrDefault();
        }

        public static bool IsTrackingTypeExist(string receiptNumber, ResiTracking.TrackingTypes trackingType, bool isJustPostedIncluded = false)
        {
            return getPostedResiTrackings(receiptNumber, isJustPostedIncluded).Where(a => a.TrackingType == trackingType.ToString()).Any();
        }

        public static bool IsTrackingTypeExist(string receiptNumber, List<string> trackingTypes, bool isJustPostedIncluded = false)
        {
            return getPostedResiTrackings(receiptNumber, isJustPostedIncluded).Where(a => trackingTypes.Contains(a.TrackingType)).Any();
        }

        public static List<ResiTracking> GetResiTrackings(string receiptNumber, List<string> trackingTypes, bool isJustPostedIncluded = false)
        {
            return getPostedResiTrackings(receiptNumber, isJustPostedIncluded).Where(a => trackingTypes.Contains(a.TrackingType)).ToList();
        }

        public static PostStats SetPartnerStat(ResiTracking tracking)
        {
            var _mapperResult = PartnerSetting.GetStatusMapper(tracking);
            if (_mapperResult != null)
            {
                tracking.PartnerTrackingType = _mapperResult.PartnerStat;
                if (_mapperResult.IsAddSeqInfo == CommonConstanta.TRUE_VAL && tracking.SameStatSeqNo > 1)
                    tracking.PartnerTrackingType = _mapperResult.PartnerStat + $", {tracking.SameStatSeqNo}";

                if (_mapperResult.MaxPost >= tracking.SameStatSeqNo && _mapperResult.IsSkipped == CommonConstanta.FALSE_VAL)
                    return PostStats.SENT;
                else
                    return PostStats.SKIP;
            }
            else
            {
                tracking.PartnerTrackingType = tracking.TrackingType;
                return PostStats.SENT;
            }
        }

        #region private methods
        private static List<ResiTracking> getPostedResiTrackings(string receiptNumber, bool isJustPostedIncluded = false)
        {
            if (isJustPostedIncluded)
            {
                var _list = ListOfResiTracking.Where(a => a.ReceiptNumber == receiptNumber
                                                && a.IsPosted)
                                                .OrderBy(x => x.TrackingDatetime).ToList();
                return _list;
            }
            else
            {
                var _list = ListOfResiTracking.Where(a => a.ReceiptNumber == receiptNumber
                                                && !string.IsNullOrEmpty(a.PostClientHistoryId)
                                                && a.PostClientHistoryStat != PostClientHistory.PostStats.ERROR.ToString())
                                                .OrderBy(x => x.TrackingDatetime).ToList();
                return _list;
            }
        }
        #endregion
    }
}
