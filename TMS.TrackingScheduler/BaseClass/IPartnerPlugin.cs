﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMS.TrackingScheduler._customObj;

namespace TMS.TrackingScheduler.BaseClass
{
    public interface IPartnerPlugin
    {
        //void SetPartnerName();
        string Post(string serializedObj);
        void SetAdditionalInfo(string serializedObj);
    }
}
