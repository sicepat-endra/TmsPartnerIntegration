﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TMS.TrackingScheduler._customObj;
using TMS.TrackingScheduler.BaseClass;
//using static TMS.TrackingScheduler._customObj.APIParam;

namespace TMS.Integration.Partner.Lazada
{
    public class PostToPartnerService : IPartnerPlugin
    {
        string _partnerName = "Lazada";
        public string Post(string serializedObj)
        {
            string _responseString = string.Empty;
            if (_partnerName.Equals(PartnerSetting.PartnerName))
            {
                ResiTracking _tracking = JsonConvert.DeserializeObject<ResiTracking>(serializedObj);
                var PostData = new Dictionary<string, string>();
                PostData["receipt_number"] = _tracking.ReceiptNumber;
                PostData["status"] = _tracking.PartnerTrackingType;
                PostData["timestamp"] = _tracking.TrackingDatetime.ToString("yyyy-MM-dd HH:mm:ss");
                PostData["description"] = _tracking.Notes;
                PostData["weight"] = _tracking.Weight > 0 ? _tracking.Weight.ToString() : string.Empty;
                PostData["location"] = _tracking.BranchName;
                PostData["height"] = _tracking.Height > 0 ? _tracking.Height.ToString() : string.Empty;
                PostData["width"] = _tracking.Width > 0 ? _tracking.Width.ToString() : string.Empty;
                PostData["length"] = _tracking.Length > 0 ? _tracking.Length.ToString() : string.Empty;
                PostData["comments"] = PartnerSetting.ListOfStatusMapper.Where(a => a.InternalStat == _tracking.TrackingType && a.IsProblem == "1").Any() ? _tracking.TrackingType : string.Empty;

                string url = PartnerSetting.APIURL;
                var client = new HttpClient();
                if (PartnerSetting.IsUseHeaderAuth == CommonConstanta.TRUE_VAL)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(PartnerSetting.HeaderAuthName, PartnerSetting.HeaderAuthKey);
                var content = new FormUrlEncodedContent(PostData);
                var response = client.PostAsync(url, content);
                _responseString = response.Result.StatusCode.ToString();
            }
            return _responseString;
        }

        public void SetAdditionalInfo(string serializedObj)
        {
            string _resp = string.Empty;
        }
    }
}

#region Old Method
//var _custPackageId = PartnerSetting.PartnerAPIParams.Where(a => a.Param == Params.CustPackageId.ToString()).FirstOrDefault();
//if (_custPackageId != null)
//{
//    if (_custPackageId.IsSent == CommonConstanta.TRUE_VAL)
//        PostData[_custPackageId.PartnerParamName] = _tracking.CustPackageId;
//}

//var _receiptNumber = PartnerSetting.PartnerAPIParams.Where(a => a.Param == Params.ReceiptNumber.ToString()).FirstOrDefault();
//if (_receiptNumber != null)
//{
//    if (_receiptNumber.IsSent == CommonConstanta.TRUE_VAL)
//        PostData[_receiptNumber.PartnerParamName] = _tracking.ReceiptNumber;
//}

//var _partnerTrackingType = PartnerSetting.PartnerAPIParams.Where(a => a.Param == Params.PartnerTrackingType.ToString()).FirstOrDefault();
//if (_partnerTrackingType != null)
//{
//    if (_partnerTrackingType.IsSent == CommonConstanta.TRUE_VAL)
//        PostData[_partnerTrackingType.PartnerParamName] = _tracking.PartnerTrackingType;
//}

//var _trackingNotes = PartnerSetting.PartnerAPIParams.Where(a => a.Param == Params.TrackingNotes.ToString()).FirstOrDefault();
//if (_trackingNotes != null)
//{
//    if (_trackingNotes.IsSent == CommonConstanta.TRUE_VAL)
//        PostData[_trackingNotes.PartnerParamName] = _tracking.Notes;
//}

//var _trackingDatetime = PartnerSetting.PartnerAPIParams.Where(a => a.Param == Params.TrackingDatetime.ToString()).FirstOrDefault();
//if (_trackingDatetime != null)
//{
//    if (_trackingDatetime.IsSent == CommonConstanta.TRUE_VAL)
//        PostData[_trackingDatetime.PartnerParamName] = Convert.ToDateTime(_tracking.TrackingDatetime).ToString("yyyy-MM-dd HH:mm:ss");
//} 
#endregion
