﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TMS.Integration.Partner.Tokopedia._customObj;
using TMS.TrackingScheduler._customObj;
using TMS.TrackingScheduler.BaseClass;
using static TMS.TrackingScheduler._customObj.ResiTracking;

namespace TMS.Integration.Partner.Tokopedia
{
    public class PostToPartnerService : IPartnerPlugin
    {
        public enum TokopediaTrackType
        {
            picked,
            dropoff
        }

        string _partnerName = "Tokopedia";
        public string Post(string serializedObj)
        {
            string _responseString = string.Empty;
            if (_partnerName.Equals(PartnerSetting.PartnerName))
            {
                ResiTracking _tracking = JsonConvert.DeserializeObject<ResiTracking>(serializedObj);
                if (_tracking.PartnerTrackingType == TrackingTypes.PICK.ToString())
                {
                    HttpClient _client = new HttpClient();
                    string url = PartnerSetting.APIURL;

                    Dictionary<string, PairContents> _content = new Dictionary<string, PairContents>();
                    PairContents _contents = new PairContents();
                    _contents.type = TokopediaTrackType.picked.ToString();
                    _contents.tracking_ref_no = _tracking.ReceiptNumber;
                    _contents.shipped_time = _tracking.TrackingDatetime.ToString("yyyy-MM-dd HH:mm:ss");
                    _content.Add("data", _contents);

                    var request = new HttpRequestMessage(HttpMethod.Post, $"{url}/pair");
                    var content = JsonConvert.SerializeObject(_content);
                    request.Content = new StringContent(content, Encoding.UTF8, "application/json");
                    request.Headers.Add("authorization-id", PartnerSetting.HeaderAuthName);
                    request.Headers.TryAddWithoutValidation("authorization", PartnerSetting.HeaderAuthKey);

                    var response = _client.SendAsync(request).Result;
                    _responseString = response.StatusCode.ToString();
                }
                else if (_tracking.PartnerTrackingType == TrackingTypes.IN.ToString())
                {
                    HttpClient _client = new HttpClient();
                    string url = PartnerSetting.APIURL;

                    Dictionary<string, PairContents> _content = new Dictionary<string, PairContents>();
                    PairContents _contents = new PairContents();
                    _contents.type = TokopediaTrackType.dropoff.ToString();
                    _contents.tracking_ref_no = _tracking.ReceiptNumber;
                    _contents.shipped_time = _tracking.TrackingDatetime.ToString("yyyy-MM-dd HH:mm:ss");
                    _content.Add("data", _contents);

                    var request = new HttpRequestMessage(HttpMethod.Post, $"{url}/pair");
                    var content = JsonConvert.SerializeObject(_content);
                    request.Content = new StringContent(content, Encoding.UTF8, "application/json");
                    request.Headers.Add("authorization-id", PartnerSetting.HeaderAuthName);
                    request.Headers.TryAddWithoutValidation("authorization", PartnerSetting.HeaderAuthKey);

                    var response = _client.SendAsync(request).Result;
                    _responseString = response.StatusCode.ToString();
                }
                else if (_tracking.ShippingFee > 0)
                {
                    HttpClient _client = new HttpClient();
                    string url = PartnerSetting.APIURL;

                    Dictionary<string, WeightContents> _content = new Dictionary<string, WeightContents>();
                    WeightContents _contents = new WeightContents();
                    _contents.tracking_ref_no = _tracking.ReceiptNumber;
                    var _oldWeight = ResiTrackings.AdditionalInfos.Select($"ReceiptNumber = '{_tracking.ReceiptNumber}'").FirstOrDefault()["EstWeight"].ToString();
                    if (!string.IsNullOrEmpty(_oldWeight))
                        _contents.old_weight = Convert.ToInt32(Convert.ToDecimal(_oldWeight) * 1000);
                    var _oldShippingFee = ResiTrackings.AdditionalInfos.Select($"ReceiptNumber = '{_tracking.ReceiptNumber}'").FirstOrDefault()["EstShippingFee"].ToString();
                    if (!string.IsNullOrEmpty(_oldShippingFee))
                        _contents.old_price = Convert.ToInt32(Convert.ToDecimal(_oldShippingFee));
                    _contents.new_price = Convert.ToInt32(Convert.ToDecimal(_tracking.ShippingFee));
                    _contents.new_weight = Convert.ToInt32(Convert.ToDecimal(_tracking.Weight) * 1000);
                    _content.Add("data", _contents);

                    var request = new HttpRequestMessage(HttpMethod.Post, $"{url}/weight");
                    var content = JsonConvert.SerializeObject(_content);
                    request.Content = new StringContent(content, Encoding.UTF8, "application/json");
                    request.Headers.Add("authorization-id", PartnerSetting.HeaderAuthName);
                    request.Headers.TryAddWithoutValidation("authorization", PartnerSetting.HeaderAuthKey);

                    var response = _client.SendAsync(request).Result;
                    _responseString = response.StatusCode.ToString();
                }
            }
            return _responseString;
        }

        public void SetAdditionalInfo(string serializedObj)
        {
            var _receiptNumbers = serializedObj.Replace("\"", "'").Replace("[", "").Replace("]", "");
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SiCepatPickupConn"].ConnectionString);
            try
            {
                //string _addCondition = buildConditionBasedOnArgs(args);
                string _cmdString = @"  select	ReceiptNumber,
			                                    total_weight as EstWeight,
			                                    est_shipping_fee as EstShippingFee
	                                    from	partnerRequestExt pre (nolock)
	                                    where	receiptnumber in 
	                                    (" + _receiptNumbers + ")";

                SqlCommand cmd = new SqlCommand(_cmdString, conn);
                cmd.CommandTimeout = 420;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ResiTrackings.AdditionalInfos);
            }
            catch (Exception e)
            {
                //writeLog($"SQL Error => {e.Message.ToString()}");
                //exitConsole();
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }
    }
}
