﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.Integration.Partner.Tokopedia._customObj
{
    public class PairModel
    {
        public List<PairContents> data { get; set; } = new List<PairContents>();
    }

    public class PairContents
    {
        public string type { get; set; }
        public string tracking_ref_no { get; set; }
        public string shipped_time { get; set; }
    }
}
