﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.Integration.Partner.Tokopedia._customObj
{
    public class WeightModel
    {
        public List<WeightContents> data { get; set; } = new List<WeightContents>();
    }

    public class WeightContents
    {
        public string tracking_ref_no { get; set; }
        public int old_weight { get; set; } = 0;
        public int old_price { get; set; } = 0;
        public int new_weight { get; set; } = 0;
        public int new_price { get; set; } = 0;
    }
}
