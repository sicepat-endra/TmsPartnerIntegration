﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TMS.Integration.Partner.Shopee._customObj;
using TMS.TrackingScheduler._customObj;
using TMS.TrackingScheduler.BaseClass;
using static TMS.TrackingScheduler._customObj.ResiTracking;

namespace TMS.Integration.Partner.Shopee
{
    public class PostToPartnerService : IPartnerPlugin
    {
        string _partnerName = "Shopee";
        public string Post(string serializedObj)
        {
            string _responseString = string.Empty;
            if (_partnerName.Equals(PartnerSetting.PartnerName))
            {
                ResiTracking _tracking = JsonConvert.DeserializeObject<ResiTracking>(serializedObj);
                var _trackings = ResiTrackings.GetResiTrackings(_tracking.ReceiptNumber);

                var _pickupTime = _trackings.Where(a => a.PartnerTrackingType == TrackingTypes.IN.ToString()).FirstOrDefault(); //assumed that when IN status = pickupTime

                TrackingPostMdl _trackingMdl = new TrackingPostMdl();
                _trackingMdl.actualPickupTimestamp = getUnixTime(_pickupTime.TrackingDatetime).ToString();
                _trackingMdl.actualWeight = _tracking.Weight > 0 ? _tracking.Weight.ToString() : string.Empty;
                _trackingMdl.airwayBillNumber = _tracking.ReceiptNumber;
                if (_tracking.ResiInputDt != null)
                    _trackingMdl.airwayBillPrintDate = getUnixTime(Convert.ToDateTime(_tracking.ResiInputDt)).ToString();
                _trackingMdl.destinationCode = _tracking.DestCode;

                _trackingMdl.insuranceAmount = _tracking.InsFee > 0 ? _tracking.InsFee.ToString() : string.Empty;
                _trackingMdl.originCode = _tracking.OriginDestCode;
                _trackingMdl.pricePerKg = _tracking.PricePerKg > 0 ? _tracking.PricePerKg.ToString() : string.Empty;
                _trackingMdl.recipientAddress = ResiTrackings.AdditionalInfos.Select($"ReceiptNumber = '{_tracking.ReceiptNumber}'").FirstOrDefault()["RecipientAddr"].ToString();
                _trackingMdl.recipientName = _tracking.RecipientName;
                _trackingMdl.senderAddress = ResiTrackings.AdditionalInfos.Select($"ReceiptNumber = '{_tracking.ReceiptNumber}'").FirstOrDefault()["SenderAddr"].ToString();
                _trackingMdl.senderName = _tracking.SenderName;
                _trackingMdl.serviceType = _tracking.ServiceType;
                _trackingMdl.shippingCost = _tracking.ShippingFee > 0 ? _tracking.ShippingFee.ToString() : string.Empty;

                TrackingStat _trackStat = new TrackingStat();
                _trackStat.additionalInfo = string.Empty;
                _trackStat.city = _tracking.BranchCity;
                _trackStat.hub = _tracking.BranchName;
                _trackStat.receiverName = _tracking.PartnerTrackingType == TrackingTypes.DLV.ToString() ? ResiTrackings.AdditionalInfos.Select($"ReceiptNumber = '{_tracking.ReceiptNumber}'").FirstOrDefault()["ReceiverName"].ToString() : string.Empty;
                _trackStat.receiverRelation = _tracking.PartnerTrackingType == TrackingTypes.DLV.ToString() ? ResiTrackings.AdditionalInfos.Select($"ReceiptNumber = '{_tracking.ReceiptNumber}'").FirstOrDefault()["ReceiverRemark"].ToString() : string.Empty;
                _trackStat.status = _tracking.PartnerTrackingType == TrackingTypes.DLV.ToString() ? "Delivered" : "On Process";
                _trackStat.statusDescription = _tracking.Notes;
                _trackStat.statusTimestamp = getUnixTime(_tracking.TrackingDatetime).ToString();

                _trackingMdl.status.Add(_trackStat);

                string url = PartnerSetting.APIURL;
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (PartnerSetting.IsUseHeaderAuth == CommonConstanta.TRUE_VAL)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(PartnerSetting.HeaderAuthName, PartnerSetting.HeaderAuthKey);
                var _json = JsonConvert.SerializeObject(_trackingMdl, Formatting.Indented);
                //var _httpContent = new StringContent(_json);

                //var response = client.PostAsync(url, new StringContent(_json, Encoding.UTF8, "application/json")).ConfigureAwait(false);
                //var response = client.PostAsync(url, _httpContent);
                //_responseString = response.Result.StatusCode.ToString();
            }

            return _responseString;
        }

        public void SetAdditionalInfo(string serializedObj)
        {
            var _receiptNumbers = serializedObj.Replace("\"", "'").Replace("[","").Replace("]", "");
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SiCepatPickupConn"].ConnectionString);
            try
            {
                //string _addCondition = buildConditionBasedOnArgs(args);
                string _cmdString = @"  select	*
                                        from 
                                        (
                                        select	ReceiptNumber,
		                                        pre.Id as PartnerRequestExtId,
                                                dh.ReceiverName as ReceiverName,
                                                dh.ReceiverRemark as ReceiverRemark
		                                        total_weight as EstWeight,
		                                        recipient_address as RecipientAddr,
		                                        shipper_address as SenderAddr,
		                                        ROW_NUMBER ( )  OVER ( PARTITION BY pre.ReceiptNumber  order by pre.dtmupd desc) as orderid
                                        from	partnerRequestExt pre (nolock)
                                        left join	receivedresi rr (nolock)
                                        on rr.NoResi = pre.receiptnumber
                                        left join	deliveryhistory dh (nolock)
                                        on dh.receivedresiid = rr.id
                                        where	receiptnumber in 
                                        (
                                            " + _receiptNumbers +
                                       ")) t where orderid = 1";

                SqlCommand cmd = new SqlCommand(_cmdString, conn);
                cmd.CommandTimeout = 420;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ResiTrackings.AdditionalInfos);
            }
            catch (Exception e)
            {
                //writeLog($"SQL Error => {e.Message.ToString()}");
                //exitConsole();
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        private long getUnixTime(DateTime dtm)
        {
            var dateTimeOffset = new DateTimeOffset(dtm);
            return dateTimeOffset.ToUnixTimeSeconds() * 1000; //based on request from shopee's documentation
        }
    }
}
