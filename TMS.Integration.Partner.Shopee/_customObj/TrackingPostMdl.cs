﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.Integration.Partner.Shopee._customObj
{
    public class TrackingPostMdl
    {
        public string airwayBillNumber { get; set; }
        public string senderName { get; set; }
        public string senderAddress { get; set; }
        public string recipientName { get; set; }
        public string recipientAddress { get; set; }
        public string airwayBillPrintDate { get; set; }
        public string originCode { get; set; }
        public string destinationCode { get; set; }
        public string actualPickupTimestamp { get; set; }
        public string actualWeight { get; set; }
        public string shippingCost { get; set; }
        public string serviceType { get; set; }
        public string pricePerKg { get; set; }
        public string insuranceAmount { get; set; }
        public List<TrackingStat> status { get; set; } = new  List<TrackingStat>();
    }

    public class TrackingStat
    {
        public string status { get; set; }
        public string statusDescription { get; set; }
        public string additionalInfo { get; set; }
        public string statusTimestamp { get; set; }
        public string city { get; set; }
        public string hub { get; set; }
        public string receiverName { get; set; }
        public string receiverRelation { get; set; }
    }
}
